<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| Your site name,
|
*/
$config['site_name'] = 'Alumarator';
$config['version'] = '3.0.0';
