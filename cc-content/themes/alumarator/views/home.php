<?= get_header(); ?>
<body id="page-top">
   <?= get_navigation(); ?>

     <header class="height-100">
      <div class="header-content" >
         <div class="header-content-inner">
            <h1 id="homeHeading">CARISAYA.ID</h1>
            <hr>
            <p class="homepage_title"><?= $homepage_title; ?></p>
            <p class="homepage_description"><?= $homepage_description; ?></p>
         </div>
      </div>
   </header>

   <footer class="footer-container">
      <p class="footer-content">
         <?= $homepage_footer; ?>
      </p>
   </footer>
   <?= get_footer(); ?>