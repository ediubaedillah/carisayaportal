<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class General extends Admin {

    public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
    }

    public function index()
	{
        $this->is_allowed('configuration_general');
		$this->data['homepage_title'] = get_option('homepage_title');
		$this->data['homepage_description'] = get_option('homepage_description');
		$this->data['homepage_footer'] = get_option('homepage_footer');
		$this->render('backend/standart/administrator/general/general', $this->data);
	}

	public function save()
	{
		if (!$this->is_allowed('setting_update', false)) {
			return $this->response([
				'success' => false,
				'message' => 'Sorry you do not have permission to setting'
			]);
		}

		$this->form_validation->set_rules('header_title', 'Header Title', 'trim', []);
		$this->form_validation->set_rules('header_description', 'Header Description', 'trim', []);

		if ($this->form_validation->run()) {
			app()->cc_app->eventListen('save_setting', $this);
			set_option('homepage_title', $this->input->post('homepage_title'));
			set_option('homepage_description', $this->input->post('homepage_description'));
			set_option('homepage_footer', $this->input->post('homepage_footer'));

			$this->response['success'] = true;
			$this->response['message'] = 'Your setting has been successfully updated.';
		} else {
			$this->response['success'] = false;
			$this->response['message'] = validation_errors();
		}

		return $this->response($this->response);
	}
}

/* End of file General.php */
