<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="<?= BASE_ASSET; ?>flag-icon/css/flag-icon.css" rel="stylesheet" media="all" />


<style type="text/css">
  .tab-pane {
    padding: 10px 0;
  }

  .legend-title {
    margin-bottom: 30px;
  }

  .legend-title .title {
    background: #fff;
    font-size: 18px;
    position: relative;
    padding: 0 10px;
    bottom: -13px;
    color: #646464;
    text-decoration: underline;
  }

  .legend-title b {
    color: #6CA45B
  }
</style>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= cclang('configuration') ?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?= cclang('configuration'); ?></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">

    <div class="col-md-12">
      <div class="box box-warning">
        <div class="box-body ">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2" style="box-shadow: none;">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header ">
              <div class="row pull-right">

              </div>
              <div class="widget-user-image">
                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">
                <?= cclang('general') ?>
              </h3>
            </div>
          </div>

          <div class="row-fluid">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="<?= empty($this->input->get('tab')) ? 'active' : '' ?>"><a href="#tab_general" class="tab_group" data-toggle="tab"><i class="fa fa-compass text-green"></i> <?= cclang('other'); ?></a></li>
                <?php if(false) app()->cc_app->initialize()->renderTabSetting(); ?>
              </ul>
              <div class="tab-content">

                <?= app()->cc_app->initialize()->renderTabContent() ?>

                <div class="tab-pane <?= empty($this->input->get('tab')) ? 'active' : '' ?>" id="tab_general">
                  <?= form_open('administrator/general/save', [
                    'name'    => 'form_setting_other',
                    'class'   => 'form-horizontal',
                    'id'      => 'form_setting_other',
                    'method'  => 'POST'
                  ]); ?>
                  <div class="row">
                    <div class="col-sm-12 col-md-8">
                      <label for="location"><?= 'Homepage Title' ?></label>
                      <input type="text" name="homepage_title" id="homepage_title" class="form-control" value="<?= $homepage_title; ?>">
                    </div>
                  </div>
                  <div class="mr-t-b-10"></div>
                  <div class="row">
                    <div class="col-sm-12 col-md-8">
                      <label for="location"><?= 'Homepage Description' ?></label>
                      <textarea type="text" name="homepage_description" id="homepage_description" class="form-control"><?= $homepage_description; ?></textarea>
                    </div>
                  </div>
                  <div class="mr-t-b-10"></div>
                  <div class="row">
                    <div class="col-sm-12 col-md-8">
                      <label for="location"><?= 'Homepage Footer' ?></label>
                      <textarea type="text" name="homepage_footer" id="homepage_footer" class="form-control"><?= $homepage_footer ?></textarea>
                    </div>
                  </div>
                  <br />
                  <?php is_allowed('setting_update', function () { ?>
                    <button class="btn btn-flat btn-default btn_save btn_action" id="btn_save" data-stype='stay' title="save"><i class="fa fa-save"></i> <?= cclang('save_button'); ?></button>
                  <?php }) ?>
                  <?= form_close(); ?>
                </div>
                <!-- /.tab-pane -->

              </div>
              <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
            <div class="message no-message-padding">

            </div>
            
            <span class="loading loading-hide"><img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> <i><?= cclang('loading_data'); ?></i></span>

            <a class="btn btn-flat btn-default btn_undo" data-id="0" id="btn_undo" title="undo (Ctrl+X)" style="display: none;"><i class="fa fa-undo"></i> Undo</a>
          </div>
          <!-- /.col -->
        </div>
      </div>
      <!--/box body -->
    </div>
    <!--/box -->

  </div>
  </div>
</section>
<!-- /.content -->
<script src="<?= BASE_ASSET; ?>ckeditor/ckeditor.js"></script>
<script>
  $(document).ready(function() {
    CKEDITOR.replace('homepage_description');
    CKEDITOR.replace('homepage_footer');
    var homepage_description = CKEDITOR.instances.homepage_description;
    var homepage_footer = CKEDITOR.instances.homepage_footer;

    $('.btn_save').click(function(e) {
      $('.message').fadeOut();
      $('#homepage_description').val(homepage_description.getData());
      $('#homepage_footer').val(homepage_footer.getData());
      var form_setting_other = $('#form_setting_other');
      var data_post = form_setting_other.serialize();

      $('.loading').show();

      $.ajax({
          url: BASE_URL + '/administrator/general/save',
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          if (res.success) {
            $('.message').printMessage({
              title: '<?= cclang('success') ?>',
              message: res.message
            });
            $('.message').fadeIn();
            $('.btn_undo').hide();

          } else {
            $('.message').printMessage({
              message: res.message,
              type: 'warning',
              title: 'Warning',
            });
            $('.message').fadeIn();
          }

        })
        .fail(function() {
          $('.message').printMessage({
            message: "<?= cclang('an_error_was_encountered') ?>",
            type: 'warning',
            title: "<?= cclang('changes_not_saved'); ?>"
          });
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({
            scrollTop: $(document).height()
          }, 1000);
        });

      return false;
    }); /*end btn save*/

  }); /*end doc ready*/

  function initialize_chosen()
  {
    $('#hq_location_chosen').css('width', '599px');
  }
</script>