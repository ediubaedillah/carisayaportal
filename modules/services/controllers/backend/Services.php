<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Services Controller
*| --------------------------------------------------------------------------
*| Services site
*|
*/
class Services extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_services');
	}

	/**
	* show all Servicess
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('services_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['servicess'] = $this->model_services->get($filter, $field, $this->limit_page, $offset);
		$this->data['services_counts'] = $this->model_services->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/services/index/',
			'total_rows'   => $this->model_services->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Services List');
		$this->render('backend/standart/administrator/services/services_list', $this->data);
	}
	
	/**
	* Add new servicess
	*
	*/
	public function add()
	{
		$this->is_allowed('services_add');

		$this->template->title('Services New');
		$this->render('backend/standart/administrator/services/services_add', $this->data);
	}

	/**
	* Add New Servicess
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('services_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('service_name', 'Service Name', 'trim|required|max_length[200]');
		

		if ($this->form_validation->run()) {
			$services_service_image_uuid = $this->input->post('services_service_image_uuid');
			$services_service_image_name = $this->input->post('services_service_image_name');
		
			$save_data = [
				'service_name' => $this->input->post('service_name'),
				'service_description' => $this->input->post('service_description'),
			];

			if (!is_dir(FCPATH . '/uploads/services/')) {
				mkdir(FCPATH . '/uploads/services/');
			}

			if (!empty($services_service_image_name)) {
				$services_service_image_name_copy = date('YmdHis') . '-' . $services_service_image_name;

				rename(FCPATH . 'uploads/tmp/' . $services_service_image_uuid . '/' . $services_service_image_name, 
						FCPATH . 'uploads/services/' . $services_service_image_name_copy);

				if (!is_file(FCPATH . '/uploads/services/' . $services_service_image_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['service_image'] = $services_service_image_name_copy;
			}
		
			
			$save_services = $this->model_services->store($save_data);

			if ($save_services) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_services;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/services/edit/' . $save_services, 'Edit Services'),
						anchor('administrator/services', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/services/edit/' . $save_services, 'Edit Services')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/services');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/services');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Servicess
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('services_update');

		$this->data['services'] = $this->model_services->find($id);

		$this->template->title('Services Update');
		$this->render('backend/standart/administrator/services/services_update', $this->data);
	}

	/**
	* Update Servicess
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('services_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('service_name', 'Service Name', 'trim|required|max_length[200]');
		
		if ($this->form_validation->run()) {
			$services_service_image_uuid = $this->input->post('services_service_image_uuid');
			$services_service_image_name = $this->input->post('services_service_image_name');
		
			$save_data = [
				'service_name' => $this->input->post('service_name'),
				'service_description' => $this->input->post('service_description'),
			];

			if (!is_dir(FCPATH . '/uploads/services/')) {
				mkdir(FCPATH . '/uploads/services/');
			}

			if (!empty($services_service_image_uuid)) {
				$services_service_image_name_copy = date('YmdHis') . '-' . $services_service_image_name;

				rename(FCPATH . 'uploads/tmp/' . $services_service_image_uuid . '/' . $services_service_image_name, 
						FCPATH . 'uploads/services/' . $services_service_image_name_copy);

				if (!is_file(FCPATH . '/uploads/services/' . $services_service_image_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['service_image'] = $services_service_image_name_copy;
			}
		
			
			$save_services = $this->model_services->change($id, $save_data);

			if ($save_services) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/services', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/services');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/services');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Servicess
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('services_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'services'), 'success');
        } else {
            set_message(cclang('error_delete', 'services'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Servicess
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('services_view');

		$this->data['services'] = $this->model_services->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Services Detail');
		$this->render('backend/standart/administrator/services/services_view', $this->data);
	}
	
	/**
	* delete Servicess
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$services = $this->model_services->find($id);

		if (!empty($services->service_image)) {
			$path = FCPATH . '/uploads/services/' . $services->service_image;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_services->remove($id);
	}
	
	/**
	* Upload Image Services	* 
	* @return JSON
	*/
	public function upload_service_image_file()
	{
		if (!$this->is_allowed('services_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'services',
		]);
	}

	/**
	* Delete Image Services	* 
	* @return JSON
	*/
	public function delete_service_image_file($uuid)
	{
		if (!$this->is_allowed('services_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'service_image', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'services',
            'primary_key'       => 'sercive_id',
            'upload_path'       => 'uploads/services/'
        ]);
	}

	/**
	* Get Image Services	* 
	* @return JSON
	*/
	public function get_service_image_file($id)
	{
		if (!$this->is_allowed('services_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$services = $this->model_services->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'service_image', 
            'table_name'        => 'services',
            'primary_key'       => 'sercive_id',
            'upload_path'       => 'uploads/services/',
            'delete_endpoint'   => 'administrator/services/delete_service_image_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('services_export');

		$this->model_services->export('services', 'services');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('services_export');

		$this->model_services->pdf('services', 'services');
	}
}


/* End of file services.php */
/* Location: ./application/controllers/administrator/Services.php */